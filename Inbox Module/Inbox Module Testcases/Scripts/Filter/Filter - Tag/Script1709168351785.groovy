import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (1)'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password (1)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/span_account_circle (1)'))

WebUI.click(findTestObject('Object Repository/Page_Home/li_groupsSwitch Team (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Home/input_search_r9 (1)'), '36dce712-a9eb-4275-96d8-dcaf74c17cc5')

WebUI.sendKeys(findTestObject('Object Repository/Page_Home/input_search_r9 (1)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/button_ChatDaddy Bug TestingChatDaddy Bug Testing (1)'))

WebUI.click(findTestObject('Object Repository/Page_Home/div_Home_MuiGrid-root css-1d2hfx (1)'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Inbox (1)'))

WebUI.click(findTestObject('Object Repository/Page_(315) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.click(findTestObject('Object Repository/Page_(315) Inbox/button_Tagarrow_drop_down'))

WebUI.setText(findTestObject('Object Repository/Page_(315) Inbox/input_search_r8'), tagSearch)

WebUI.sendKeys(findTestObject('Object Repository/Page_(315) Inbox/input_search_r8'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_(315) Inbox/input_Katalon Studio Test1_PrivateSwitchBas_588651'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(315) Inbox/input_Katalon Studio Test1_PrivateSwitchBas_588651'), 
    Keys.chord(Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_Inbox/span_Ivan'))

WebUI.rightClick(findTestObject('Object Repository/Page_Inbox - Ivan/span_Ivan'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/li_portraitContact Info'))

WebUI.verifyTextPresent(expectedtag, true)

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_account_circle'))

WebUI.click(findTestObject('Object Repository/Page_Inbox - Ivan/span_Logout'))

WebUI.closeBrowser()

