import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.beta.chatdaddy.tech/Login')

WebUI.setText(findTestObject('Page_ChatDaddy/input__r0'), '9205212092')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password'), 'bl/ynKqD2d1DDaEllGeQPQ==')

WebUI.sendKeys(findTestObject('Object Repository/Page_ChatDaddy/input_Password_password'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/span_account_circle'))

WebUI.click(findTestObject('Object Repository/Page_Home/li_groupsSwitch Team'))

WebUI.setText(findTestObject('Object Repository/Page_Home/input_search_r9'), '36dce712-a9eb-4275-96d8-dcaf74c17cc5')

WebUI.sendKeys(findTestObject('Object Repository/Page_Home/input_search_r9'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_Home/button_ChatDaddy Bug TestingChatDaddy Bug Testing'))

WebUI.click(findTestObject('Object Repository/Page_Home/div_Home_MuiGrid-root css-1d2hfx'))

WebUI.click(findTestObject('Object Repository/Page_Home/span_Inbox'))

WebUI.click(findTestObject('Object Repository/Page_(308) Inbox/span_Skip this Short and Sweet Tour'))

WebUI.click(findTestObject('Object Repository/Page_(308) Inbox/button_Channelarrow_drop_down'))

WebUI.setText(findTestObject('Object Repository/Page_(308) Inbox/input_search_r8'), channelSearch)

WebUI.sendKeys(findTestObject('Object Repository/Page_(308) Inbox/input_search_r8'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_(308) Inbox/input_QA WABA_PrivateSwitchBase-input css-1m9pwf3'))

WebUI.sendKeys(findTestObject('Object Repository/Page_(308) Inbox/input_QA WABA_PrivateSwitchBase-input css-1m9pwf3'), Keys.chord(
        Keys.ESCAPE))

WebUI.click(findTestObject('Object Repository/Page_(3) Inbox/span_TNT The Next Tech Sdn. Bhd'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_(3) Inbox - TNT The Next Tech Sdn. Bhd/span_601113219100'), 
    expectedChannel)

WebUI.click(findTestObject('Object Repository/Page_(3) Inbox - TNT The Next Tech Sdn. Bhd/span_account_circle'))

WebUI.click(findTestObject('Object Repository/Page_(3) Inbox - TNT The Next Tech Sdn. Bhd/span_Logout'))

WebUI.closeBrowser()

