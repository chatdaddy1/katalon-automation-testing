<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Sorry We couldnt find the chat you searched for</name>
   <tag></tag>
   <elementGuidId>1d13a72e-8e0a-4289-a585-e6f448baf394</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div[3]/div[2]/div/div[2]/div[3]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.MuiTypography-root.MuiTypography-body2.css-9pm7v1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>4ad81bbe-132a-4cd9-80f4-b061586667c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>MuiTypography-root MuiTypography-body2 css-9pm7v1</value>
      <webElementGuid>f2714d6c-2082-495e-804e-5260d98f5a10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Sorry! We couldn’t find the chat you searched for.</value>
      <webElementGuid>b34884d2-d4bc-44ba-a8ca-e4cb45eddab5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;MuiGrid-root MuiGrid-container MuiGrid-direction-xs-column MuiGrid-wrap-xs-nowrap css-5u791s&quot;]/div[@class=&quot;MuiGrid-root css-qrdc5g&quot;]/div[@class=&quot;MuiGrid-root css-1r3ro9p&quot;]/div[@class=&quot;MuiGrid-root css-148h8t7&quot;]/div[@class=&quot;MuiGrid-root css-12u48ha&quot;]/p[@class=&quot;MuiTypography-root MuiTypography-body2 css-9pm7v1&quot;]</value>
      <webElementGuid>85bc90e7-65da-41f0-81e3-0621fb275983</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div[3]/div[2]/div/div[2]/div[3]/p</value>
      <webElementGuid>dada6b84-ba56-4a9c-abed-580f0b1d3862</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulk Actions'])[1]/following::p[1]</value>
      <webElementGuid>a7365139-19c7-42ca-8c4b-d23a49be2f72</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='arrow_drop_down'])[4]/following::p[2]</value>
      <webElementGuid>d52af2a8-3a1c-4c39-9bd4-8468c4c49386</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[1]/preceding::p[1]</value>
      <webElementGuid>9357c750-bf67-488d-8fb6-16adeb387da3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Messages Instead?'])[2]/preceding::p[1]</value>
      <webElementGuid>270841fe-54d9-4e00-bf9e-101c78801631</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sorry! We couldn’t find the chat you searched for.']/parent::*</value>
      <webElementGuid>33a1db62-ed1c-49ce-ab42-0d25a87a6772</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/p</value>
      <webElementGuid>2848cbc4-a2f1-40ba-9279-b31459a9a3de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Sorry! We couldn’t find the chat you searched for.' or . = 'Sorry! We couldn’t find the chat you searched for.')]</value>
      <webElementGuid>0bd48f18-68d5-46af-af16-bdffbefb24df</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
